﻿Student stuGrade = new Student();
int choice = 0;
do
{
    Menu();
    choice = int.Parse(Console.ReadLine());
    Console.Clear();
    switch (choice)
    {
        case 1:
            Console.WriteLine("Enter student id: ");
            stuGrade.Id = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter student name: ");
            stuGrade.Name = Console.ReadLine();
            Console.WriteLine("Enter student Age: ");
            stuGrade.Age = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter student GPA: ");
            stuGrade.GPA = double.Parse(Console.ReadLine());
            break;
        case 2:
            Grade(stuGrade);
            break;
        case 3:
            View(stuGrade);
            break;
        case 4:
            Console.WriteLine("Cam on ban da dung ung dung");
            Environment.Exit(0);
            break;
        default:
            Console.WriteLine("Nhap so theo bang tren");
            break;
    }

} while (choice != 4);

void Grade(Student student)
{
    Console.WriteLine("Xep loai hoc sinh: ");
    if (student.GPA >= 8.0)
    {
        Console.WriteLine("Hoc sinh gioi");
    }
    else if (student.GPA < 8.0 && student.GPA >= 6.5)
    {
        Console.WriteLine("Hoc sinh kha");
    }
    else if (student.GPA < 6.5 && student.GPA >= 5.0)
    {
        Console.WriteLine("Hoc sinh trung binh");
    }
    else
    {
        Console.WriteLine("Hoc sinh yeu");
    }
}

void View(Student student)
{
    Console.WriteLine("Student id: " + student.Id);
    Console.WriteLine("Student name: " + student.Name);
    Console.WriteLine("Student age: " + student.Age);
    Console.WriteLine("Student gpa: " + student.GPA);
}

void Menu()
{
    Console.WriteLine("---Student management----");
    Console.WriteLine("1. Nhap thong tin sinh vien");
    Console.WriteLine("2. Hien thi diem.");
    Console.WriteLine("3. Hien thi thong tin sinh vien");
    Console.WriteLine("4. Thoat");
}

class Student
{
    public int Id;
    public string Name;
    public int Age;
    public double GPA;
}